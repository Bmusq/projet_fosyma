package eu.su.mas.dedaleEtu.mas.knowledge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.graphstream.algorithm.Dijkstra;
import org.graphstream.graph.Edge;
import org.graphstream.graph.EdgeRejectedException;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.fx_viewer.FxViewer;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.Viewer.CloseFramePolicy;

import dataStructures.serializableGraph.*;
import dataStructures.tuple.Couple;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import javafx.application.Platform;
import javafx.util.Pair;

/**
 * <pre>
 * This simple topology representation only deals with the graph, not its content.
 * The knowledge representation is not well written (at all), it is just given as a minimal example.
 * The viewer methods are not independent of the data structure, and the dijkstra is recomputed every-time.
 * </pre>
 * @author hc, modified by: basile musquer, marius le chapelier
 */
public class MapRepresentation implements Serializable {

	/**
	 * A node is open, closed, or agent
	 * @author hc
	 *
	 */

	public enum MapAttribute {
		agent,open,closed
	}

	private static final long serialVersionUID = -1333959882640838272L;

	/*********************************
	 * Parameters for graph rendering
	 ********************************/

	private String defaultNodeStyle= "node {"+"fill-color: black;"+" size-mode:fit;text-alignment:under; text-size:14;text-color:white;text-background-mode:rounded-box;text-background-color:black;}";
	private String nodeStyle_open = "node.agent {"+"fill-color: forestgreen;"+"}";
	private String nodeStyle_agent = "node.open {"+"fill-color: blue;"+"}";
	private String nodeStyle=defaultNodeStyle+nodeStyle_agent+nodeStyle_open;

	private Graph g; //data structure non serializable
	private Viewer viewer; //ref to the display,  non serializable
	private Integer nbEdges;//used to generate the edges ids

	private SerializableSimpleGraph<String, MapAttribute> sg;//used as a temporary dataStructure during migration


	public MapRepresentation() {
		//System.setProperty("org.graphstream.ui.renderer","org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		System.setProperty("org.graphstream.ui", "javafx");
		this.g= new SingleGraph("My world vision");
		this.g.setAttribute("ui.stylesheet",nodeStyle);

		Platform.runLater(() -> {
			openGui();
		});
		//this.viewer = this.g.display();

		this.nbEdges=0;
	}

	/**
	 * Add or replace a node and its attribute 
	 * @param id Id of the node
	 * @param mapAttribute associated state of the node
	 */
	public void addNode(String id,MapAttribute mapAttribute){
		Node n;
		if (this.g.getNode(id)==null){
			n=this.g.addNode(id);
		}else{
			n=this.g.getNode(id);
		}
		n.clearAttributes();
		n.setAttribute("ui.class", mapAttribute.toString());
		n.setAttribute("ui.label",id);
	}
	/**
	 * Add the edge if not already existing.
	 * @param idNode1 one side of the edge
	 * @param idNode2 the other side of the edge
	 */
	public void addEdge(String idNode1,String idNode2){
		try {
			this.nbEdges++;
			this.g.addEdge(this.nbEdges.toString(), idNode1, idNode2);
		}catch (EdgeRejectedException e){
			//Do not add an already existing one
			this.nbEdges--;
		}

	}
	
	public Couple<List<String>, List<String>> getNodes(){
		List<String> openNodes = new ArrayList<String>();
		List<String> closedNodes = new ArrayList<String>();
		
		Iterator<Node> iter=this.g.iterator();
		while(iter.hasNext()){
            Node n=iter.next();
            if (MapAttribute.valueOf((String)n.getAttribute("ui.class")) == MapAttribute.closed) {
            	closedNodes.add(n.getId());
            } else {
            	openNodes.add(n.getId());
            }
        }
		return new Couple<>(openNodes, closedNodes);
	}
	

	/**
	 * Return Map's data
	 */
	public SerializableSimpleGraph<String, MapAttribute> getSerialiazableData(Couple<List<String>, List<String>> memory, Couple<List<String>, List<String>> nodes){
		List<String> differencesOpen = new ArrayList<>();
		List<String> differencesClosed = new ArrayList<>();
		if (memory != null) {
			differencesOpen = new ArrayList<>(nodes.getLeft());
//			differencesOpen.removeAll(memory.getLeft());
			
			differencesClosed = new ArrayList<>(nodes.getRight());
			differencesClosed.removeAll(memory.getRight());
		}
		SerializableSimpleGraph<String, MapAttribute> data = new SerializableSimpleGraph<String,MapAttribute>();
        Iterator<Node> iter=this.g.iterator();
        while(iter.hasNext()){
            Node n=iter.next();
            if(differencesOpen.contains(n.getId()) || differencesClosed.contains(n.getId())) { // si le noeud vient d'être ajouté
//            if(differencesClosed.contains(n.getId())) {
            		data.addNode(n.getId(), MapAttribute.valueOf((String)n.getAttribute("ui.class")));
            }
        }
        Iterator<Edge> iterE=this.g.edges().iterator();
        while (iterE.hasNext()){
            Edge e=iterE.next();
            Node sn=e.getSourceNode();
            Node tn=e.getTargetNode();
            // Si l'un ou l'autre est nouveau alors, l'edge est nouvelle.
            if( (differencesClosed.contains(sn.getId()) || differencesOpen.contains(sn.getId())) && (differencesClosed.contains(tn.getId()) || differencesOpen.contains(tn.getId())) ) { // les deux noeuds sont dans l'une des deux listes
            	//System.out.println(e.getId() + sn.getId() +  tn.getId());
                data.addEdge(e.getId(), sn.getId(), tn.getId());
            }
        }
        return data;
    }// TODO: Do not send the wall map everytime, select data to send
	 // TODO: Simplify the data send

	/**
	 * Merge data from another agent with own data
	 */
	public void merge(SerializableSimpleGraph<String, MapAttribute> data){
        for (SerializableNode<String, MapAttribute> nodeMessage: data.getAllNodes()) {
        	Node tmp = this.g.getNode(nodeMessage.getNodeId());
        	if(tmp == null || tmp.getAttribute("ui.class").equals(MapAttribute.open.toString())) {
        		addNode(nodeMessage.getNodeId(), MapAttribute.valueOf(nodeMessage.getNodeContent().toString()));
        	}
        }
        for (SerializableNode<String, MapAttribute> nodeMessage: data.getAllNodes()) {
        	for(String s : data.getEdges(nodeMessage.getNodeId())){
        		addEdge(nodeMessage.getNodeId(), s);
        	}
        }
	}

	/**
	 * Compute the shortest Path from idFrom to IdTo. The computation is currently not very efficient
	 * 
	 * @param idFrom id of the origin node
	 * @param idTo id of the destination node
	 * @return the list of nodes to follow
	 */
	public List<String> getShortestPath(String idFrom,String idTo){
		List<String> shortestPath=new ArrayList<String>();
		Map<String,String> visited= new HashMap<String, String>();
		ArrayList<String> queue = new ArrayList<String>();
		boolean done = false;
		String s = null;
		
        Iterator<Node> iter=this.g.iterator();
        while(iter.hasNext()){
            Node n=iter.next();
            visited.put(n.getId(), "_");
        }
        visited.replace(idFrom, idFrom);
		queue.add(idFrom);
		while(!done && !queue.isEmpty()) {
			String curr = queue.remove(0);
			for(Edge e : this.g.getNode(curr).edges().collect(Collectors.toList())) {
				if( (s = e.getTargetNode().getId()).equals(curr) ) {
					s=e.getSourceNode().getId();
				}
	        	if(visited.get(s).equals("_")) {
	        		visited.replace(s, curr);
	        		queue.add(s);
	        		if(s.equals(idTo)) {
	        			done = true;	
	        			break;
	        		}
	        	}
			}
		}
		String curr = idTo;
		while(!curr.equals(idFrom)) {
			shortestPath.add(0,curr);
			curr = visited.get(curr);
		}
		System.out.println(shortestPath);
		return shortestPath;
	}

	/**
	 * Before the migration we kill all non serializable components and store their data in a serializable form
	 */
	public void prepareMigration(){
		this.sg= new SerializableSimpleGraph<String,MapAttribute>();
		Iterator<Node> iter=this.g.iterator();
		while(iter.hasNext()){
			Node n=iter.next();
			sg.addNode(n.getId(),(MapAttribute)n.getAttribute("ui.class"));
		}
		Iterator<Edge> iterE=this.g.edges().iterator();
		while (iterE.hasNext()){
			Edge e=iterE.next();
			Node sn=e.getSourceNode();
			Node tn=e.getTargetNode();
			sg.addEdge(e.getId(), sn.getId(), tn.getId());
		}

		closeGui();

		this.g=null;

	}

	/**
	 * After migration we load the serialized data and recreate the non serializable components (Gui,..)
	 */
	public void loadSavedData(){

		this.g= new SingleGraph("My world vision");
		this.g.setAttribute("ui.stylesheet",nodeStyle);

		openGui();

		Integer nbEd=0;
		for (SerializableNode<String, MapAttribute> n: this.sg.getAllNodes()){
			this.g.addNode(n.getNodeId()).setAttribute("ui.class", n.getNodeContent().toString());
			for(String s:this.sg.getEdges(n.getNodeId())){
				this.g.addEdge(nbEd.toString(),n.getNodeId(),s);
				nbEd++;
			}
		}
		System.out.println("Loading done");
	}

	/**
	 * Method called before migration to kill all non serializable graphStream components
	 */
	private void closeGui() {
		//once the graph is saved, clear non serializable components
		if (this.viewer!=null){
			try{
				this.viewer.close();
			}catch(NullPointerException e){
				System.err.println("Bug graphstream viewer.close() work-around - https://github.com/graphstream/gs-core/issues/150");
			}
			this.viewer=null;
		}
	}

	/**
	 * Method called after a migration to reopen GUI components
	 */
	private void openGui() {
		this.viewer =new FxViewer(this.g, FxViewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);////GRAPH_IN_GUI_THREAD);
		viewer.enableAutoLayout();
		viewer.setCloseFramePolicy(FxViewer.CloseFramePolicy.CLOSE_VIEWER);
		viewer.addDefaultView(true);
		g.display();
	}
}