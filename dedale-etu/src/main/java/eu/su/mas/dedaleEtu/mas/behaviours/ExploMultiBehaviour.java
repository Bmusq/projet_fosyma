package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.own.ExploreMultiAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.util.leap.*;
import javafx.util.Pair;

/**
 * This behavior allows an agent to explore the environment and learn the associated topological map.
 * It "pings" its surroundings to exchange data with another agent if any is around
 * 
 * Warning, this behaviour does not save the content of visited nodes, only the topology.</br> 
 * @author basile musquer, marius le chapelier
 *
 */
public class ExploMultiBehaviour extends SimpleBehaviour {

	private static final long serialVersionUID = 8567687745496787661L;

	private boolean finished = false;

	/**
	 * Current knowledge of the agent regarding the environment
	 */
	private MapRepresentation myMap;

	/**
	 * Nodes known but not yet visited
	 */
	private List<String> openNodes;
	/**
	 * Visited nodes
	 */
	private List<String> closedNodes;
	
	/**
	 * 
	 */
	private boolean termination;
	
	/**
	 * List of closed nodes communicated to each agent.
	 */
	private Hashtable<String, Couple<List<String>, List<String>>> communicationMemory;
	
	/**
	 * 
	 * @param myagent
	 * @param myMap
	 */
	public ExploMultiBehaviour(final AbstractDedaleAgent myagent, MapRepresentation myMap, Hashtable<String, Couple<List<String>, List<String>>> communicationMemory) {
		super(myagent);
		this.myMap=myMap;
		this.termination = false;
		if (communicationMemory != null) {
			this.communicationMemory = communicationMemory;
		} else {
			this.communicationMemory = new Hashtable<>();
		}	
		if (myMap != null) {
			Couple<List<String>, List<String>> nodes = myMap.getNodes();
			this.openNodes = nodes.getLeft();
			this.closedNodes = nodes.getRight();
		} else {
			this.openNodes=new ArrayList<String>();
			this.closedNodes=new ArrayList<String>();
		}
	}

	@Override
	public void action() {
		System.out.println(this.myAgent.getLocalName() + "EXPLOOO");
		if(this.myMap==null)
			this.myMap= new MapRepresentation();
		
		// Ping
		final ACLMessage outmsg = new ACLMessage(ACLMessage.INFORM);
		outmsg.setProtocol("PingProtocol");
		outmsg.setSender(this.myAgent.getAID());
		outmsg.clearAllReceiver();
		DFAgentDescription[] result = ((ExploreMultiAgent) this.myAgent).getAgentDescription("Explore");
		for (int i = 0; i < result.length; i++) {
			if(! result[i].getName().getLocalName().equals(this.myAgent.getLocalName())) {
				if (((ExploreMultiAgent) this.myAgent).checkTimer(result[i].getName().getLocalName())) {
					outmsg.addReceiver(new AID(result[i].getName().getLocalName(), AID.ISLOCALNAME));
				}
			}
		}			
		outmsg.setContent("ping");
		((AbstractDedaleAgent) this.myAgent).sendMessage(outmsg);
		// End ping
		
		// Listen to ping
		MessageTemplate msgTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),MessageTemplate.MatchProtocol("PingProtocol"));
		ACLMessage inmsg= this.myAgent.receive(msgTemplate);
		if(inmsg != null) {
			if (((ExploreMultiAgent) myAgent).checkTimer(inmsg.getSender().getLocalName())){
				System.out.println(this.myAgent.getLocalName()+ "<----Message received from "+inmsg.getSender().getLocalName()+" ,content= "+inmsg.getContent()+" in ExploMultiBehaviour");
				switch(inmsg.getContent()) {
					case "ping":
		                outmsg.clearAllReceiver();
		                outmsg.addReceiver(new AID(inmsg.getSender().getLocalName(), AID.ISLOCALNAME));
		                outmsg.setContent("ack");
		                ((AbstractDedaleAgent) this.myAgent).sendMessage(outmsg);
		                
		                msgTemplate = MessageTemplate.and(
		                        MessageTemplate.MatchPerformative(ACLMessage.INFORM),
		                        MessageTemplate.and(MessageTemplate.MatchSender(inmsg.getSender()),
		                        MessageTemplate.MatchProtocol("PingProtocol")));
		                inmsg= this.myAgent.blockingReceive(msgTemplate, 500); // Wait 0.1 s
		                if(inmsg != null && inmsg.getContent().equals("ack")) {
		                    System.out.println(this.myAgent.getLocalName()+ "<----Message received from "+inmsg.getSender().getLocalName()+" ,content= "+inmsg.getContent());
		                    this.myAgent.addBehaviour(new CommunicateMultiBehaviour((AbstractDedaleAgent) this.myAgent, this.myMap, inmsg.getSender().getLocalName(), this.communicationMemory));
		                    this.finished=true;
		                    this.done();
		                }
		                
		                break;
		            case "ack": // Should not happen except if some ping message got lost
		                outmsg.clearAllReceiver();
		                outmsg.addReceiver(new AID(inmsg.getSender().getLocalName(), AID.ISLOCALNAME));
		                outmsg.setContent("ack");
		                ((AbstractDedaleAgent) this.myAgent).sendMessage(outmsg);
		                this.myAgent.addBehaviour(new CommunicateMultiBehaviour((AbstractDedaleAgent) this.myAgent, this.myMap, inmsg.getSender().getLocalName(), this.communicationMemory));
		                this.finished=true;
		                this.done();
		                break;
				}
			}
		}
	
		//0) Retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		String nextNode = null;
		if (myPosition!=null && this.termination==false){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition

			/**
			 * Just added here to let you see what the agent is doing, otherwise he will be too quick
			 */
			try {
				this.myAgent.doWait(500);
			} catch (Exception e) {
				e.printStackTrace();
			}

			//1) remove the current node from openlist and add it to closedNodes.
			this.closedNodes.add(myPosition);
			this.openNodes.remove(myPosition);

			this.myMap.addNode(myPosition,MapAttribute.closed);

			//2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
            Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();
            List<String> potentialNextMove = new ArrayList<String>();
            while(iter.hasNext()){
            	Couple<String, List<Couple<Observation, Integer>>> a = iter.next();
                String nodeId = a.getLeft();
                if (!this.closedNodes.contains(nodeId)){
//                	if(a.getRight().size() != 0) System.out.println("RIGHT"+a.getRight());
                    potentialNextMove.add(nodeId);
                    if (!this.openNodes.contains(nodeId)){
                        this.openNodes.add(nodeId);
                        this.myMap.addNode(nodeId, MapAttribute.open);
                        this.myMap.addEdge(myPosition, nodeId);    
                    }else{
                        //the node exist, but not necessarily the edge
                        this.myMap.addEdge(myPosition, nodeId);
                    }
                }
            }
            
            if (potentialNextMove.size() > 0) {
                Random rand = new Random();
                nextNode = potentialNextMove.get(rand.nextInt(potentialNextMove.size()));
            }
		}
		//3) while openNodes is not empty, continues.
        if (this.openNodes.isEmpty()){
          if(!termination){
            try { DFService.deregister((ExploreMultiAgent) this.myAgent); }
            catch (Exception e) {}
            
            this.termination=true;
            ServiceDescription sd = new ServiceDescription();
            sd.setType("Waiting");
            sd.setName(((AbstractDedaleAgent)this.myAgent).getLocalName());
            ((ExploreMultiAgent) this.myAgent).register(sd);
          }
            //Explo finished
        if(((ExploreMultiAgent) this.myAgent).getAgentDescription("Explore").length == 0) { // No more agents are exploring
                finished=true;
                try { DFService.deregister((ExploreMultiAgent) this.myAgent); }
                catch (Exception e) {}
                System.out.println("Exploration successufully done, behaviour removed.");
                // this.myAgent.addBehaviour(new HuntMultiBehaviour((AbstractDedaleAgent)this.myAgent, "My parameters"));
            }
        }else{
            //4) select next move.
			//4.1 If there exist one open node directly reachable, go for it,
			//	 otherwise choose one from the openNode list, compute the shortestPath and go for it
			if (nextNode==null){
				//no directly accessible openNode
				//chose one, compute the path and take the first step.
				nextNode=this.myMap.getShortestPath(myPosition, this.openNodes.get((int) Math.random() * this.openNodes.size())).get(0);
				// TODO: modifier le choix du prochain noeuds + synchro avec autre agent si échange de donnée (min ou max de distance de Hamming)
			}
			

			/************************************************
			 * 				END API CALL ILUSTRATION
			 *************************************************/
			((AbstractDedaleAgent)this.myAgent).moveTo(nextNode);
		}
	}
	
	@Override
	public boolean done() {
		return finished;
	}

}
