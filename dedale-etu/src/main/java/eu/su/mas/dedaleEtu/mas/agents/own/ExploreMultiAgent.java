package eu.su.mas.dedaleEtu.mas.agents.own;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.ExploMultiBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
//import jade.core.AID;
//import jade.domain.AMSService;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.*;
/**
 * <pre>
 * ExploreMulti agent. 
 * It explore the map using TODO
 * It stops when all nodes have been visited.
 *  </pre>
 *  
 * @author basile musquer, marius le chapelier
 *
 */

public class ExploreMultiAgent extends AbstractDedaleAgent {

	private static final long serialVersionUID = -6431796765590433727L;
	private MapRepresentation myMap;
	private HashMap<String, Date> exchangesTimer;
	private final long Communication_Couldown = 10000; // 10 secondes ?

	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviors
	 *          
	 */
	
	public ExploreMultiAgent() {
		super();
		this.exchangesTimer = new HashMap<String, Date>();
	}
	
	protected void setup(){

		super.setup();
		
		final Object[] args = getArguments();
		if(args.length!=2){
			System.err.println("[Error] - two parameters expected");
			System.exit(-1);
		} else {
			 ServiceDescription sd = new ServiceDescription();
			 sd.setType("Explore");
			 sd.setName(getLocalName());
			 register(sd);
			
			List<Behaviour> lb=new ArrayList<Behaviour>();
			
			/************************************************
			 * 
			 * ADD the initial behaviors of the Agent here
			 * 
			 ************************************************/
			
			lb.add(new ExploMultiBehaviour(this, this.myMap, null));
			 
			
			/***
			 * MANDATORY TO ALLOW YOUR AGENT TO BE DEPLOYED CORRECTLY
			 */
			
			
			addBehaviour(new startMyBehaviours(this,lb));
			
			System.out.println("the  agent "+this.getLocalName()+ " is started");
		}
	}
	
	 public void register(ServiceDescription sd) {
		  DFAgentDescription dfd = new DFAgentDescription();
		  dfd.setName(getAID());
		  dfd.addServices(sd);

		  try {
		   DFService.register(this, dfd);
		  } catch (FIPAException fe) {
		   fe.printStackTrace();
		  }
	 }
	 
	 public DFAgentDescription[] getAgentDescription(String s) {
			DFAgentDescription dfd = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();

			sd.setType(s);
			dfd.addServices(sd);
			DFAgentDescription[] result=null;
			try {
				result = DFService.search(this, dfd);
			} catch (FIPAException fe) {
				   fe.printStackTrace();
			}
			return result;
		}
	 
	 public void resetTimer(String id) {
		 exchangesTimer.remove(id);
		 exchangesTimer.put(id, new Date());
	 }
	 
	 public boolean checkTimer(String id) {
		 Date timer = exchangesTimer.get(id);
//		 System.out.println("last time the agent communicate : " + timer);
//		 System.out.println("now : " + new Date());
		 if (timer == null) {
//			 System.out.println("they haven't met yet");
			 return true;
		 } else if (new Date().getTime() - timer.getTime() > this.Communication_Couldown){
//			 System.out.println("they talked a long time ago");
			 return true;
		 }
//		 System.out.println("Cooldown");
		 return false;
	 }
}
